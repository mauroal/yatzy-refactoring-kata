import java.util.Arrays;
import java.util.stream.IntStream;

public class Yatzy {

  private final DiceService diceService;

  private final int[] dice;

  public Yatzy(int d1, int d2, int d3, int d4, int d5) {
    dice = new int[5];
    dice[0] = d1;
    dice[1] = d2;
    dice[2] = d3;
    dice[3] = d4;
    dice[4] = d5;

    diceService = new DiceService();
  }

  public int chance() {
    return Arrays.stream(dice).sum();
  }

  public int yatzy() {
    if (sumSameOfAKind(5) != 0) {
      return 50;
    }
    return 0;
  }

  public int ones() {
    return sumDiceEqualsToValue(1);
  }

  public int twos() {
    return sumDiceEqualsToValue(2);
  }

  public int threes() {
    return sumDiceEqualsToValue(3);
  }

  public int fours() {
    return sumDiceEqualsToValue(4);
  }

  public int fives() {
    return sumDiceEqualsToValue(5);
  }

  public int sixes() {
    return sumDiceEqualsToValue(6);
  }

  public int pair() {
    return sumSameOfAKind(2);
  }

  public int twoPairs() {
    Tallies tallies = new Tallies(dice);

    return diceService.sumForEachFaceMatchingCondition(diceValue -> tallies.getByDiceValue(diceValue) >= 2, 2) * 2;
  }

  public int threeOfAKind() {
    return sumSameOfAKind(3);
  }

  public int fourOfAKind() {
    return sumSameOfAKind(4);
  }

  public int smallStraight() {
    Tallies tallies = new Tallies(dice);

    if (IntStream.rangeClosed(1, 5).allMatch(diceValue -> tallies.getByDiceValue(diceValue) == 1)) {
      return 15;
    }
    return 0;
  }

  public int largeStraight() {
    Tallies tallies = new Tallies(dice);

    if (IntStream.rangeClosed(2, 6).allMatch(diceValue -> tallies.getByDiceValue(diceValue) == 1)) {
      return 20;
    }
    return 0;
  }

  public int fullHouse() {
    Tallies tallies = new Tallies(dice);

    int doubleDiceValue = diceService.sumForEachFaceMatchingCondition(diceValue -> tallies.getByDiceValue(diceValue) == 2);
    int tripletDiceValue = diceService.sumForEachFaceMatchingCondition(diceValue -> tallies.getByDiceValue(diceValue) == 3);

    if (doubleDiceValue > 0 && tripletDiceValue > 0) {
      return doubleDiceValue * 2 + tripletDiceValue * 3;
    }
    return 0;
  }

  private int sumDiceEqualsToValue(int valueToCompare) {
    return Arrays.stream(dice)
      .filter(die -> die == valueToCompare)
      .sum();
  }

  private int sumSameOfAKind(int numberOfDiceMatching) {
    Tallies tallies = new Tallies(dice);

    return diceService.sumForEachFaceMatchingCondition(diceValue -> tallies.getByDiceValue(diceValue) >= numberOfDiceMatching) * numberOfDiceMatching;
  }
}
