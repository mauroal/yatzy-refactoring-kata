import java.util.function.IntPredicate;

public class DiceService {

  public int sumForEachFaceMatchingCondition(IntPredicate condition) {
    return sumForEachFaceMatchingCondition(condition, 1);
  }

  public int sumForEachFaceMatchingCondition(IntPredicate condition, int matchCountExpected) {
    int diceValue = 0;
    int matchCount = 0;

    for (int currentDiceValue = 6; currentDiceValue > 0; currentDiceValue--) {
      if (condition.test(currentDiceValue)) {
        matchCount++;
        diceValue += currentDiceValue;

        if (matchCount >= matchCountExpected) {
          return diceValue;
        }
      }
    }

    return 0;
  }
}
