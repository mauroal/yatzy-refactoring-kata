import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiceServiceTest {

  @Test
  void sumForEachDiceFaceMatchingCondition_shouldScoreFirstDiceFaceMatchingCondition_whenMatchCountExpectedIsNotGiven() {
    // Given
    DiceService diceService = new DiceService();

    // When
    int actual = diceService.sumForEachFaceMatchingCondition(diceValue -> diceValue < 3);

    // Then
    assertEquals(2, actual);
  }

  @Test
  void sumForEachDiceFaceMatchingCondition_shouldScore0_whenMatchCountExpectedIsNotGivenAndConditionIsAlwaysFalse() {
    // Given
    DiceService diceService = new DiceService();

    // When
    int actual = diceService.sumForEachFaceMatchingCondition(diceValue -> diceValue == 8);

    // When
    assertEquals(0, actual);
  }

  @ParameterizedTest(name = "With matchCountExpected = {0} and expectedResult = {1}")
  @CsvSource({
    "5, 19",
    "4, 18",
    "3, 15",
    "2, 11",
    "1, 6"
  })
  void sumForEachDiceFaceMatchingCondition_shouldScoreForEachDiceFaceMatchingCondition_whenMatchCountExpectedIsGiven(int matchCountExpected, int expectedResult) {
    // Given
    DiceService diceService = new DiceService();

    // When
    int actual = diceService.sumForEachFaceMatchingCondition(diceValue -> diceValue != 2, matchCountExpected);

    // Then
    assertEquals(expectedResult, actual);
  }

  @Test
  void sumForEachDiceFaceMatchingCondition_shouldScore0_whenMatchCountExpectedIsGivenAndConditionIsAlwaysFalse() {
    // Given
    DiceService diceService = new DiceService();

    // When
    int actual = diceService.sumForEachFaceMatchingCondition(diceValue -> diceValue == 8, 1);

    // Then
    assertEquals(0, actual);
  }

  @Test
  void sumForEachDiceFaceMatchingCondition_shouldScore0_whenMatchCountExpectedIsGivenAndMatchingCountExpectedIsNotReached() {
    // Given
    DiceService diceService = new DiceService();

    // When
    int actual = diceService.sumForEachFaceMatchingCondition(diceValue -> diceValue > 5, 2);

    // Then
    assertEquals(0, actual);
  }
}
