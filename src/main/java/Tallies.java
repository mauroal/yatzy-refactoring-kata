import java.util.HashMap;
import java.util.Map;

public class Tallies {

  private final Map<Integer, Integer> tallies;

  public Tallies(int[] dice) {
    tallies = new HashMap<>();

    for (int die : dice) {
      tallies.put(die, tallies.getOrDefault(die, 0) + 1);
    }
  }

  public int getByDiceValue(int dice) {
    return tallies.getOrDefault(dice, 0);
  }
}
