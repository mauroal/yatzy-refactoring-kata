import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TalliesTest {

  @Test
  void constructor_shouldConstructTalliesWithDice() {
    // Given
    int[] dice = {1, 5, 3, 3, 6};

    // When
    Tallies tallies = new Tallies(dice);

    // Then
    assertEquals(1, tallies.getByDiceValue(1));
    assertEquals(0, tallies.getByDiceValue(2));
    assertEquals(2, tallies.getByDiceValue(3));
    assertEquals(0, tallies.getByDiceValue(4));
    assertEquals(1, tallies.getByDiceValue(5));
    assertEquals(1, tallies.getByDiceValue(6));
  }

  @Test
  void getByDiceValue_shouldReturnTallieOfDiceValue() {
    // Given
    int[] dice = {2, 5, 3, 2, 2};
    Tallies tallies = new Tallies(dice);

    // When
    int actual = tallies.getByDiceValue(2);

    // Then
    assertEquals(3, actual);
  }

  @Test
  void getByDiceValue_shouldReturn0_whenTalliesDoesNotContainsDiceValue() {
    // Given
    int[] dice = {2, 5, 3, 2, 2};
    Tallies tallies = new Tallies(dice);

    // When
    int actual = tallies.getByDiceValue(6);

    // Then
    assertEquals(0, actual);
  }
}
