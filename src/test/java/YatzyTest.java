import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class YatzyTest {

  @Test
  void chance_shouldScoreSumOfAllDice() {
    // Then
    assertEquals(14, new Yatzy(1, 1, 3, 3, 6).chance());
    assertEquals(21, new Yatzy(4, 5, 5, 6, 1).chance());
    assertEquals(15, new Yatzy(2, 3, 4, 5, 1).chance());
    assertEquals(16, new Yatzy(3, 3, 4, 5, 1).chance());
  }

  @Test
  void yatzy_shouldScore50_whenAllDicesAreEquals() {
    // Then
    assertEquals(50, new Yatzy(1, 1, 1, 1, 1).yatzy());
    assertEquals(0, new Yatzy(1, 1, 1, 2, 1).yatzy());
    assertEquals(50, new Yatzy(4, 4, 4, 4, 4).yatzy());
    assertEquals(50, new Yatzy(6, 6, 6, 6, 6).yatzy());
    assertEquals(0, new Yatzy(6, 6, 6, 6, 3).yatzy());
  }

  @Test
  void ones_shouldScoreSumOfAllDiceOfValue1() {
    // Then
    assertEquals(0, new Yatzy(3, 3, 3, 4, 5).ones());
    assertEquals(1, new Yatzy(1, 2, 3, 4, 5).ones());
    assertEquals(2, new Yatzy(1, 2, 1, 4, 5).ones());
    assertEquals(0, new Yatzy(6, 2, 2, 4, 5).ones());
    assertEquals(4, new Yatzy(1, 2, 1, 1, 1).ones());
  }

  @Test
  void twos_shouldScoreSumOfAllDiceOfValue2() {
    // Then
    assertEquals(4, new Yatzy(2, 3, 2, 5, 1).twos());
    assertEquals(4, new Yatzy(1, 2, 3, 2, 6).twos());
    assertEquals(10, new Yatzy(2, 2, 2, 2, 2).twos());
  }

  @Test
  void threes_shouldScoreSumOfAllDiceOfValue3() {
    // Then
    assertEquals(6, new Yatzy(1, 2, 3, 2, 3).threes());
    assertEquals(12, new Yatzy(2, 3, 3, 3, 3).threes());
  }

  @Test
  void fours_shouldScoreSumOfAllDiceOfValue4() {
    // Then
    assertEquals(8, new Yatzy(1, 1, 2, 4, 4).fours());
    assertEquals(12, new Yatzy(4, 4, 4, 5, 5).fours());
    assertEquals(8, new Yatzy(4, 4, 5, 5, 5).fours());
    assertEquals(4, new Yatzy(4, 5, 5, 5, 5).fours());
  }

  @Test
  void fives_shouldScoreSumOfAllDiceOfValue5() {
    // Then
    assertEquals(10, new Yatzy(4, 4, 4, 5, 5).fives());
    assertEquals(15, new Yatzy(4, 4, 5, 5, 5).fives());
    assertEquals(20, new Yatzy(4, 5, 5, 5, 5).fives());
  }

  @Test
  void sixes_shouldScoreSumOfAllDiceOfValue6() {
    // Then
    assertEquals(0, new Yatzy(4, 4, 4, 5, 5).sixes());
    assertEquals(6, new Yatzy(4, 4, 6, 5, 5).sixes());
    assertEquals(18, new Yatzy(6, 5, 6, 6, 5).sixes());
  }

  @Test
  void pair_shouldScoreSumOfTwoHighestMatchingDice() {
    // Then
    assertEquals(8, new Yatzy(3, 3, 3, 4, 4).pair());
    assertEquals(12, new Yatzy(1, 1, 6, 2, 6).pair());
    assertEquals(6, new Yatzy(3, 3, 3, 4, 1).pair());
    assertEquals(6, new Yatzy(3, 3, 3, 3, 1).pair());
    assertEquals(6, new Yatzy(3, 4, 3, 5, 6).pair());
    assertEquals(10, new Yatzy(5, 3, 3, 3, 5).pair());
    assertEquals(12, new Yatzy(5, 3, 6, 6, 5).pair());
  }

  @Test
  void twoPairs_shouldScoreSumOfTwoPairOfTwoHighestMatchingDice() {
    // Then
    assertEquals(8, new Yatzy(1, 1, 2, 3, 3).twoPairs());
    assertEquals(0, new Yatzy(1, 1, 2, 3, 4).twoPairs());
    assertEquals(6, new Yatzy(1, 1, 2, 2, 2).twoPairs());
    assertEquals(16, new Yatzy(3, 3, 5, 4, 5).twoPairs());
    assertEquals(16, new Yatzy(3, 3, 5, 5, 5).twoPairs());
  }

  @Test
  void threeOfAKind_shouldScoreSumOfThreeHighestMatchingDice() {
    // Then
    assertEquals(9, new Yatzy(3, 3, 3, 4, 5).threeOfAKind());
    assertEquals(0, new Yatzy(3, 3, 4, 5, 6).threeOfAKind());
    assertEquals(9, new Yatzy(3, 3, 3, 3, 1).threeOfAKind());
    assertEquals(15, new Yatzy(5, 3, 5, 4, 5).threeOfAKind());
    assertEquals(9, new Yatzy(3, 3, 3, 3, 5).threeOfAKind());
  }

  @Test
  void fourOfAKind_shouldScoreSumOfFourHighestMatchingDice() {
    // Then
    assertEquals(8, new Yatzy(2, 2, 2, 2, 5).fourOfAKind());
    assertEquals(0, new Yatzy(2, 2, 2, 5, 5).fourOfAKind());
    assertEquals(8, new Yatzy(2, 2, 2, 2, 2).fourOfAKind());
    assertEquals(12, new Yatzy(3, 3, 3, 3, 5).fourOfAKind());
    assertEquals(20, new Yatzy(5, 5, 5, 4, 5).fourOfAKind());
    assertEquals(12, new Yatzy(3, 3, 3, 3, 3).fourOfAKind());
  }

  @Test
  void smallStraight_shouldScore15_whenDiceValueFollowEachOtherFrom1To5() {
    // Then
    assertEquals(15, new Yatzy(1, 2, 3, 4, 5).smallStraight());
    assertEquals(15, new Yatzy(2, 3, 4, 5, 1).smallStraight());
    assertEquals(0, new Yatzy(1, 2, 2, 4, 5).smallStraight());
  }

  @Test
  void largeStraight_shouldScore20_whenDiceValueFollowEachOtherFrom2To6() {
    // Then
    assertEquals(20, new Yatzy(6, 2, 3, 4, 5).largeStraight());
    assertEquals(20, new Yatzy(2, 3, 4, 5, 6).largeStraight());
    assertEquals(0, new Yatzy(1, 2, 2, 4, 5).largeStraight());
  }

  @Test
  void fullHouse_shouldScoreSumOfAllDice_whenDiceAreTwoOfKindAndThreeOfOtherKind() {
    // Then
    assertEquals(8, new Yatzy(1, 1, 2, 2, 2).fullHouse());
    assertEquals(0, new Yatzy(2, 2, 3, 3, 4).fullHouse());
    assertEquals(0, new Yatzy(4, 4, 4, 4, 4).fullHouse());
    assertEquals(18, new Yatzy(6, 2, 2, 2, 6).fullHouse());
    assertEquals(0, new Yatzy(2, 3, 4, 5, 6).fullHouse());
  }
}
